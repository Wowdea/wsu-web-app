import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-adminlogin',
  templateUrl: './adminlogin.component.html',
  styleUrls: ['./adminlogin.component.css']
})
export class AdminloginComponent implements OnInit {

  obj:any={};
  isInvalid:boolean=false;
  @Input() isAdmin:boolean=false
  @Output() isAdminChange=new EventEmitter<boolean>();
  isAdminDashboard:boolean=false;

  constructor() { }

  ngOnInit() {
  }

  login(){
    if(this.obj.uname=="admin"&&this.obj.password=="test1"){
      this.isAdminDashboard=true;
      this.isInvalid=false;
    }else{
      this.isInvalid=true;
      this.obj={};
    }
  }

  back(){
    this.isAdmin=false;
    this.isAdminChange.emit(this.isAdmin);
  }

}
