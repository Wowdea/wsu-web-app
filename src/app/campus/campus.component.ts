import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-campus',
  templateUrl: './campus.component.html',
  styleUrls: ['./campus.component.css']
})
export class CampusComponent implements OnInit {
  campus:any=["Parramatta","Bankstown","Liverpool"];
  
  @Input() isCampus:boolean=false
  @Output() isCampusChange=new EventEmitter<boolean>();
  selectedCampus:any;
  isCandidateList:boolean=false;

  constructor() { }

  ngOnInit() {
  }

  selectCampus(campus){
    this.selectedCampus=campus;
    this.isCandidateList=true;
  }

  back(){
    this.isCampus=false;
    this.isCampusChange.emit(this.isCampus);
  }

}
