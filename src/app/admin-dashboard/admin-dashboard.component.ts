import { Component, OnInit , Injectable, Injector} from '@angular/core';
import { CandidateService } from '../services/candidate.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Angular5Csv } from 'angular5-csv/Angular5-csv';
import { AppComponent } from "../app.component";

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  candidateList:any;
  exportdata:any;
  parentComponent:any;

  constructor(public service:CandidateService, public injector:Injector) { }

  ngOnInit() {
    this.parentComponent=this.injector.get(AppComponent);
    this.candidateList=JSON.parse(localStorage.getItem('candidatesList'));
    if(this.candidateList==null){
      this.getData();
    }
  }

  logout(){
    this.parentComponent.startedVoting=false;
    this.parentComponent.isAdminLogin=false;
  }

  prepareFormat(){
    let date=new Date();
    
    let dataArray=[]
    for(let candidate of this.candidateList){
      let obj={};

      obj['campus']=candidate.campus;
      obj['name']=candidate.name;
      obj['count']=candidate.count;
      obj['time']=date;
      dataArray.push(obj);
    }

    return dataArray

  }

  export(){
   let data= this.prepareFormat();
    var options = { 
      headers: ["Campus", "Name", "Count","Date&Time"]
    };
    new Angular5Csv(data, 'candidateVotes',options);
  }

  getData(){
    let url='../assets/JSON/candidateList.json';
    this.service.getJSON(url).subscribe(data=>{
      this.candidateList=data['candidates'];
        localStorage.setItem('candidatesList',JSON.stringify(this.candidateList));
    },(err: HttpErrorResponse)=>{
      console.log("err",err);
    })
  }

}
