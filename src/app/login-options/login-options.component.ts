import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-options',
  templateUrl: './login-options.component.html',
  styleUrls: ['./login-options.component.css']
})
export class LoginOptionsComponent implements OnInit {

  isLogin:boolean=false;
  @Input() isOptions:boolean=false;
  @Output() isOptionsChange=new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  Login(){
    this.isLogin=true;
  }

  back(){
    this.isOptions=false;
    this.isOptionsChange.emit(this.isOptions);
  }

}
