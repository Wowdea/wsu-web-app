import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { CandidateService } from '../services/candidate.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-candidates-list',
  templateUrl: './candidates-list.component.html',
  styleUrls: ['./candidates-list.component.css']
})
export class CandidatesListComponent implements OnInit {

  candidateList:any;
  campus:any;
  isSelected:boolean=false;
  selectedCandidate:any;
  @Input() selectedCampus:any;
  @Input() isCandidates:boolean=false
  @Output() isCandidatesChange=new EventEmitter<boolean>();

  constructor(public service:CandidateService) { }

  ngOnInit() {
    this.getData();
    this.campus=this.selectedCampus;
  }

  getData(){
    let c=localStorage.getItem('candidatesList');
    let url='../assets/JSON/candidateList.json';
    this.service.getJSON(url).subscribe(data=>{
      this.candidateList=data['candidates'];
      if(c==null){  
        localStorage.setItem('candidatesList',JSON.stringify(this.candidateList));
      }
    },(err: HttpErrorResponse)=>{
      console.log("err",err);
    })
  }

  onSelect(candidate){
    this.isSelected=true;
    this.selectedCandidate=candidate;
  }

  back(){
    this.isCandidates=false;
    this.isCandidatesChange.emit(this.isCandidates);
  }

}
