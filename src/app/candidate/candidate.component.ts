import { Component, OnInit, Input, Output , EventEmitter } from '@angular/core';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})
export class CandidateComponent implements OnInit {

  @Input() selectedCandidate:any;
  @Input() isSelected:boolean=false;
  candidate:any;
  isLogin:boolean=false;
  @Output() isSelectedChange = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
    this.candidate=this.selectedCandidate;
  }

  Vote(){
    this.isLogin=true;
    localStorage.setItem('candidate',JSON.stringify(this.candidate));
  }

  back(){
    this.isSelected=false;
    this.isSelectedChange.emit(this.isSelected);
  }

}
