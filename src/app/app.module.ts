import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CandidatesListComponent } from './candidates-list/candidates-list.component';
import { LoginComponent } from './login/login.component';

import { CandidateService } from "./services/candidate.service";
import { HttpClientModule } from '@angular/common/http';
import { CandidateComponent } from './candidate/candidate.component';

import {FormsModule}   from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginOptionsComponent } from './login-options/login-options.component';
import { HeaderComponent } from './header/header.component';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { CampusComponent } from './campus/campus.component';


@NgModule({
  declarations: [
    AppComponent,
    CandidatesListComponent,
    LoginComponent,
    CandidateComponent,
    DashboardComponent,
    LoginOptionsComponent,
    AdminloginComponent,
    HeaderComponent,
    AdminDashboardComponent,
    CampusComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [CandidateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
