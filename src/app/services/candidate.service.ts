import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CandidateService {

  url:string;

  constructor(private http:HttpClient ) { }

  public getJSON(url:any): Observable<any>{
    
    return this.http.get(url)
  }

}
