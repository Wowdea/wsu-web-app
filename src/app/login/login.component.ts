import { Component, OnInit, Input, Output, EventEmitter,Injectable, Injector } from '@angular/core';
import { CandidateService } from '../services/candidate.service';
import { HttpErrorResponse } from '@angular/common/http';
import { element } from 'protractor';
import { AppComponent } from "../app.component";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  parentComponent:any;
  doneVoting:boolean=false;
  @Input() isLogin:boolean=false
  @Input() candidate:any;
  isOTP:boolean=false;
  isInvalid:boolean=false;
  selectedVoteFor:any;
  usersList:any;
  obj:any={};
  selectedCandidate:object;
  candidateList:any;
  toHome:boolean=false;
  
  @Output() isLoginChange = new EventEmitter<boolean>();

  constructor(public service:CandidateService, public injector:Injector) { }

  ngOnInit() {
    this.selectedVoteFor=this.candidate;
    this.parentComponent=this.injector.get(AppComponent);
  }

  showOTP(){
    let url="../assets/JSON/users.json"

    this.service.getJSON(url).subscribe(data=>{
      this.usersList=data['users'];
      let isValid=this.checkvalid();
      if (isValid === true) {

        this.isOTP=true;
      }
      else{
        this.isInvalid=true;
        this.obj={};
      }
    })
  }

  verifyOTP(){
    if(this.obj.otp==="12345"){
      this.getData();
    }
    else{
      this.isOTP=false;
      this.isInvalid=true;
      this.obj={};
    }
  }

  getData(){
    let data=JSON.parse(localStorage.getItem('candidate'));
    if(data.count){
      data.count=data.count+1;
    }
    else{
      data.count=1;
    }

    this.selectedCandidate=data;
    
    
    let url="../assets/JSON/users.json"
    this.service.getJSON(url).subscribe(data=>{
      this.usersList=data['users'];
      let isValid=this.checkvalid();
      if(isValid==true){
        console.log("success");
        
        // this.toHome=true;
        this.updateList();
        this.doneVoting=true;
        // window.location.reload();
        
      }
      else{
        console.log("not valid");
        
      }
    },(err: HttpErrorResponse)=>{
      console.log("err",err);
    })
  }

  processCompleted(){    
    this.parentComponent.startedVoting=false;
    this.parentComponent.isAdminLogin=false;
  }

  updateList(){
    this.candidateList=JSON.parse(localStorage.getItem('candidatesList'));
    for(let candidate of this.candidateList){
      if(candidate.id==this.selectedCandidate['id']){
        candidate.count=candidate.count+1;
      }
    }
    localStorage.setItem('candidatesList', JSON.stringify(this.candidateList));
  }

  checkvalid(){
    for(let user of this.usersList){
      if(this.obj.uname==user.UserName && this.obj.password==user.Password){
        return true
      }
    }
    return false
  }

  back(){
    this.isLogin=false;
    this.isLoginChange.emit(this.isLogin);

  }
  


}
