import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
 
  departments:any=["department1","department2","department3"];
  constructor() { }
 
  ngOnInit() {
  }

}
